const path=require('path');
var express=require("express");
var router_app=require(path.join(__dirname, 'controllers', 'control.js'));
var db=require(path.join(__dirname, '..', 'db', 'database.js'));

var server=express();

server.set("view engine", "pug");

server.use(express.json());
server.use(express.urlencoded(
    { extended: false } ));

server.use("/simpleApp", router_app);
server.use("/simpleApp/static", express.static("estaticos"));

server.listen(3000);
