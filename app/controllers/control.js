var express=require("express");
var User=require("../models/user.js");

var router=express.Router();

router.route("/login").get((req,res)=>{
    res.render("../app/views/login");
}).post(async (req,res)=>{
    let st=await User.login(req.body.usuario, req.body.pwd);
    if (st==User.OK){
        res.send("LOGIN EXITOSO");
    } else {
        res.send("LOGIN NO EXITOSO");
    }
});

router.route("/usrManager").get((req,res)=>{
    res.render("../app/views/usrManager");
}).post(async (req,res)=>{
    let st=await User.addUser(req.body.usuario,req.body.pwd,req.body.pwd2);
    if (st==User.OK)
        res.send("OK");
    else
        res.send("NO OK");
});

module.exports = router;
